ree (1.4.1-3) UNRELEASED; urgency=medium

  * d/control: Generalize *-i386 *-amd64 into any-.

 -- Samuel Thibault <sthibault@debian.org>  Sun, 12 Nov 2023 21:41:05 +0100

ree (1.4.1-2) unstable; urgency=medium

  * Update maintainer address.
  * d/control: Add Vcs fields.
  * Bump standards version to 4.6.0.
  * Bump debhelper version to 13, drop d/compat.
  * Add d/upstream/metadata.
  * d/copyright: bump year.
  * d/clean: added.

 -- Gürkan Myczko <tar@debian.org>  Tue, 15 Mar 2022 16:19:11 +0100

ree (1.4.1-1) unstable; urgency=medium

  * New upstream version.
  * dump debian/ manual pages.
  * Bump standards version to 4.2.1.
  * debian/patches: dropped.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Wed, 05 Sep 2018 11:47:56 +0200

ree (1.3-4) unstable; urgency=medium

  * New maintainer. (Closes: #691828)
  * Bump debhelper version to 4.1.3.
  * Bump standards version to 11.
  * Updated homepage fields.
  * debian/watch: updated.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Wed, 24 Jan 2018 13:38:26 +0100

ree (1.3-3) unstable; urgency=medium

  [ Joao Eriberto Mota Filho ]

  * QA upload.
  * Ack to NMU. Thanks to Willi Mann <willi@debian.org>. (Closes: #675218)
  * Set Debian QA Group as maintainer. (see #691828)
  * Migrations:
      - debian/control to 1.0 format.
      - DebSrc to 3.0 version.
  * debian/control:
      - Bumped Standards-Version to 3.9.8.
      - Improved the long description.
  * debian/fontdump.1: changed the manpage level from 8 to 1 in its header.
  * debian/patches/:
      - 10_avoid-direct-changes.patch: created to avoid direct changes in
        upstream source code.
  * debian/rules: added the DEB_BUILD_MAINT_OPTIONS variable to improve the
      GCC hardening.
  * debian/watch: added a fake site to explain about the current status
      of the original upstream homepage.

  [ Logan Rosen ]

  * Applied several changes sent by Logan Rosen <logan@ubuntu.com>. Thanks!
  * Updated DH level to 9. (Closes: #817650)
  * debian/control:
      - Added ${misc:Depends} variable to Depends field.
      - Removed dead homepage URL from description. (Closes: #615464)
  * debian/patches/:
      - 20_add-gcc-hardening-fix-clean.patch:
          ~ Add -f to rm command in clean target.
          ~ Add variables to pass in hardening flags.
      - 30_fix-source-code.patch:
          ~ fontdump.c: include <strings.h> for the bcmp function.
          ~ ree.c: include <string.h> for the strncmp function.
  * debian/rules: Convert to dh sequencer.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 23 Aug 2016 19:12:00 -0300

ree (1.3-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix maintainer e-mail address - the old one bounces. closes: #675218

 -- Willi Mann <willi@debian.org>  Fri, 15 Jun 2012 21:15:42 +0200

ree (1.3-2) unstable; urgency=low

  * Add kfreebsd-amd64 to Architecture line. (Closes: #361618)

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Sun, 16 Apr 2006 20:28:23 +0200

ree (1.3-1) unstable; urgency=low

  * Initial Release. (Closes: #329455)

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Wed, 28 Apr 2004 13:35:16 +0200
